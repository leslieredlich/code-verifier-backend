import express, { Express, Request, Response } from "express";
import dotenv from 'dotenv';

//configuracion de archivo .env
dotenv.config();

// crear express app
const app: Express = express();
const port: string | number = process.env.PORT || 8000;

//define la primera ruta de la app
app.get('/', (req: Request, res: Response) => {
    res.send('Welcome to api restful: Express + TS + nodemon + Jest + VSwagger +Mongose');
});

app.get('/hello', (req: Request, res: Response) => {
    res.send('Welcome to GET Route: ¡Hello!');
});

// ejecutar App y escuchar el request en el puerto
app.listen(port, () => {
    console.log(`Express server ranning in at http://localhost:${port}`);
});