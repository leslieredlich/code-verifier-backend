"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const dotenv_1 = __importDefault(require("dotenv"));
//configuracion de archivo .env
dotenv_1.default.config();
// crear express app
const app = (0, express_1.default)();
const port = process.env.PORT || 8000;
//define la primera ruta de la app
app.get('/', (req, res) => {
    res.send('Welcome to api restful: Express + TS + nodemon + Jest + VSwagger +Mongose');
});
app.get('/hello', (req, res) => {
    res.send('Welcome to GET Route: ¡Hello!');
});
// ejecutar App y escuchar el request en el puerto
app.listen(port, () => {
    console.log(`Express server ranning in at http://localhost:${port}`);
});
//# sourceMappingURL=index.js.map